import React, { Component } from "react";
import { LinearGauge } from "react-canvas-gauges";

class Temperature extends Component {
  render() {
    const { temperature } = this.props;
    const tempOptions = {
      units: "°C",
      width: 400,
      height: 800,
      minValue: 0,
      maxValue: 50,
      value: temperature && temperature !== "na" ? temperature : 0,
      majorTicks: [0,10,20,30,40,50],
      highlights: [
        {
          from: 0,
          to: 10,
          color: "#95c83d"
        },
        {
          from: 10,
          to: 20,
          color: "#d8d831"
        },
        {
          from: 20,
          to: 30,
          color: "#e4b338"
        },
        {
          from: 30,
          to: 40,
          color: "#e68a24"
        },
        {
          from: 40,
          to: 50,
          color: "#e44b2b"
        }
      ]
    };

    return (
      <div
        style={{
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row"
        }}
      >
        <div style={{ flexGrow: 1, paddingLeft: 50 }}>
          <LinearGauge {...tempOptions} />
        </div>
        <div style={{ flexGrow: 1, fontSize: "3em", padding: "0 50px" }}>
          <h1>
            The cabin temperature of the bus is{" "}
            {temperature && temperature !== "na" ? temperature + "°C" : "N/A"}
          </h1>
        </div>
      </div>
    );
  }
}

export default Temperature;
