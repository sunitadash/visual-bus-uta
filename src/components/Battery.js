import React, { Component } from "react";

class Battery extends Component {
  constructor(props) {
    super(props);
    this.canvas = React.createRef();
  }
  componentDidMount() {
    this.updateComponent();
  }

  componentWillUpdate() {
    this.updateComponent();
  }

  updateComponent = () => {
    const charge =
      this.props.batteryLevel && this.props.batteryLevel !== "na"
        ? this.props.batteryLevel
        : 0;

    const context = this.canvas.current.getContext("2d");
    //Battery outlines
    context.beginPath();
    context.rect(5, 5, 600, 300);
    context.lineWidth = 10;
    context.strokeStyle = "black";
    context.stroke();

    context.beginPath();
    context.rect(610, 140, 10, 20);
    context.fillStyle = "red";
    context.fill();
    context.stroke();

    //Battery fill
    context.beginPath();
    context.rect(10, 10, 590 * (charge / 100), 290);
    context.fillStyle =
      "rgb(" +
      Math.floor((1 - charge / 100) * 255) +
      "," +
      Math.floor((charge / 100) * 255) +
      ",0)";
    context.fill();
  };

  render() {
    const { batteryLevel } = this.props;
    const charge =
      batteryLevel && batteryLevel !== "na"
        ? Math.floor(batteryLevel) + "%"
        : "N/A";

    return (
      <div
        style={{
          flexGrow: 1,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          height: "100vh"
        }}
      >
        <canvas ref={this.canvas} width={625} height={320} />
        <h1 style={{ fontSize: "3em" }}>
          The battery level of the bus is {charge}
        </h1>
      </div>
    );
  }
}

export default Battery;
