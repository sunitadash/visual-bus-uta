import React, { Component } from "react";
import Map from "pigeon-maps";
import Overlay from "pigeon-overlay";

import bus from "./assets/bus.png";
const Marker = () => (
  <img
    src={bus}
    alt="Marker"
    style={{ width: 50, height: 95, marginLeft: -25, marginTop: -25 }}
  />
);

class GMaps extends Component {
  render() {
    if (!this.props.lat && !this.props.lng) return null;
    const gMapsOptions = {
      center: [parseFloat(this.props.lat), parseFloat(this.props.lng)],
      zoom: 15
    };

    return (
      <div style={{ height: "100vh", width: "100%" }}>
        <Map center={gMapsOptions.center} zoom={gMapsOptions.zoom}>
          <Overlay anchor={gMapsOptions.center}>
            <Marker />
          </Overlay>
        </Map>
      </div>
    );
  }
}

export default GMaps;
