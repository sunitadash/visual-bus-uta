import React, { Component } from "react";
import { RadialGauge } from "react-canvas-gauges";

class SpeedoMeter extends Component {
  render() {
    const { spd, eps } = this.props;
    let speed = 0;
    if (spd && !isNaN(parseFloat(spd))) {
      speed = Math.floor(parseFloat(spd) * 3.6);
    }


    const opts = {
      /* Gauge customization */
      units: "KM/h",
      title: "Speed",
      value: Math.floor(speed),
      minValue: 0,
      maxValue: 100,
      width: 800,
      height: 800,
      animation: true,
      animationRule: "elastic",
      animationDuration: 500,
      highlights: [
        {
          from: 0,
          to: 20,
          color: "#95c83d"
        },
        {
          from: 20,
          to: 40,
          color: "#d8d831"
        },
        {
          from: 40,
          to: 60,
          color: "#e4b338"
        },
        {
          from: 60,
          to: 80,
          color: "#e68a24"
        },
        {
          from: 80,
          to: 100,
          color: "#e44b2b"
        }
      ]
    };
    return (
      <div
        style={{
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row"
        }}
      >
        <div style={{ flexGrow: 1, paddingLeft: 50 }}>
          <RadialGauge {...opts} />
        </div>
        <div style={{ flexGrow: 1, fontSize: "3em", padding: "0 50px" }}>
          <h1>The speed of the bus is {speed} KM/h</h1>
        </div>
      </div>
    );
  }
}

export default SpeedoMeter;
