import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Switch, Route } from "react-router";

import GMaps from "./components/GMaps";
import SpeedoMeter from "./components/SpeedoMeter";
import Battery from "./components/Battery";
import Temperature from "./components/Temperature";
import mqtt from "mqtt";

// const mqttOptions = {
//   port: 8083,
//   host: "ws://llbbroker2.northeurope.cloudapp.azure.com",
//   clientId:
//     "mqttjs_" +
//     Math.random()
//       .toString(16)
//       .substr(2, 8),
//   username: "llbpub",
//   password: "gt3r25regt",
//   encoding: "utf-8",
//   protocol: "wss"
// };

// class App extends Component {
//   state = {
//     location: {},
//     can: {}
//   };
//   componentWillMount() {
//     const busId = 1612;
//     this.locationTopic = `fi/llb/bus/${busId}/location/`;
//     this.canTopic = `fi/llb/bus/${busId}/can/`;
//     this.client = mqtt.connect(
//       mqttOptions.host,
//       mqttOptions
//     );

//     this.client.on("connect", () => {
//       console.log("connected");
//       this.client.subscribe(this.locationTopic);
//       this.client.subscribe(this.canTopic);
//       this.client.on("message", this.onMessage);
//     });
//   }

//   onMessage = (topic, message) => {
//     const data = JSON.parse(message.toString());
//     if (topic === this.locationTopic) {
//       this.setState({ location: data });
//     } else if (topic === this.canTopic) {
//       this.setState({ can: data });
//     }
//   };

//   componentWillUnmount() {
//     this.client.end();
//   }



const fetchOptions = {
  method: "get",
  headers: new Headers({
    authorization:
      "IGePNRMoZ0jLgbXZ"
  })
};

class App extends Component {
  state = {
    busData: {},
    lastKnownLocation: {
      lat: "55.658081667",
      lon: "12.556374333"
    }
  };
  componentWillMount() {
    setInterval(() => {
      fetch(
        "https://llbapi.northeurope.cloudapp.azure.com?busId=1614",
        fetchOptions
      )
        .then(res => res.json())
        .then(res => {
          let lastKnownLocation = {...this.state.lastKnownLocation}
          if(!isNaN(+res.lat) && !isNaN(+res.lon)) {
            lastKnownLocation = {
              lat: +res.lat,
              lon: +res.lon
            }
          }
          this.setState({ busData: res, lastKnownLocation })
        });
    }, 1000);
  }



  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <GMaps
                lat={this.state.lastKnownLocation.lat}
                lng={this.state.lastKnownLocation.lon}
              />
            )}
          />
          <Route
            path="/speedometer"
            render={() => (
              <SpeedoMeter
                spd={this.state.busData.spd}
                eps={this.state.busData.eps}
              />
            )}
          />
          <Route
            path="/battery"
            render={() => (
              <Battery batteryLevel={this.state.busData.can && this.state.busData.can.DD_BatteryLevel} />
            )}
          />
          <Route
            path="/temp"
            render={() => (
              <Temperature temperature={this.state.busData['1614_28'] && this.state.busData['1614_28']['set_28']} />
            )}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
